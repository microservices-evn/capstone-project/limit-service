--liquibase formatted sql
--changeset microservice_class:20230713

CREATE
EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE PRODUCT_LIMIT_CONFIG
(
    PRODUCT_ID       uuid PRIMARY KEY default uuid_generate_v4(),
    PRODUCT_CODE     varchar(255) NOT NULL UNIQUE,
    DAILY_LIMIT      numeric          DEFAULT 0,
    MONTHLY_LIMIT    numeric          DEFAULT 0,
    UPDATED_BY_EVENT uuid,
    UPDATED_AT       timestamp
);

CREATE TABLE DAILY_TRANSACTIONS
(
    ID             uuid PRIMARY KEY default uuid_generate_v4(),
    TRANSACTION_ID uuid         NOT NULL,
    FROM_CIF       varchar(255) NOT NULL,
    BENEFICIARY    varchar(255) NOT NULL,
    AMOUNT         numeric          DEFAULT 0,
    PRODUCT_ID     varchar(255),
    PRODUCT_CODE   varchar(255),
    STATUS         varchar(255), -- RESERVED/REVERTED
    CREATED_AT     timestamp,
    SAGA_ID        varchar(255)
);

CREATE TABLE MONTHLY_TRANSACTIONS
(
    ID             uuid PRIMARY KEY default uuid_generate_v4(),
    TRANSACTION_ID uuid         NOT NULL,
    FROM_CIF       varchar(255) NOT NULL,
    BENEFICIARY    varchar(255) NOT NULL,
    PRODUCT_ID     varchar(255),
    PRODUCT_CODE   varchar(255),
    STATUS         varchar(255), -- ACCUMULATED/RESERVED/CONFIRMED
    CREATED_AT     timestamp,
    SAGA_ID        varchar(255)
);

CREATE TABLE PRODUCT_EVENT
(
    EVENT_ID                uuid PRIMARY KEY,
    PRODUCT_ID              uuid,
    PRODUCT_CODE            varchar(255) NOT NULL,
    PRODUCT_LIMIT_TYPE      varchar(255) NOT NULL, -- DAILY, MONTHLY
    PRODUCT_LIMIT_AMOUNT    numeric DEFAULT 0,
    PRODUCT_LAST_UPDATED_AT timestamp,
    EVENT_CREATED_AT        timestamp
);