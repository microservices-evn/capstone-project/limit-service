--liquibase formatted sql
--changeset microservice_class:20230718_2

INSERT INTO product_limit_config (product_id, product_code, daily_limit, monthly_limit, updated_by_event, updated_at)
VALUES ('a51fea9c-ea7a-4a02-bfd4-0eec3aa55a5a', 'INTERNAL_TRANSFER', 150000000, 20000000000,
        '6164dbd3-6869-4133-90d1-362d06ba5c62', '2023-07-18 06:03:27.534'),
       ('b799f852-4ce1-4412-9248-9911bca6f32f', 'EXTERNAL_TRANSFER', 50000000, 10000000000,
        'b064e36a-7273-4249-acdb-dc1779510b24', '2023-07-18 06:04:14.641');
