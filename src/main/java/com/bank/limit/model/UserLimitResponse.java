package com.bank.limit.model;

import com.bank.limit.entity.ProductLimitConfigEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class UserLimitResponse {
    @Schema(
            name = "accumulatedDailyAmount",
            title = "accumulatedDailyAmount",
            example = "100"
    )
    private BigDecimal accumulatedDailyAmount;

    @Schema(
            name = "accumulatedMonthlyAmount",
            title = "accumulatedMonthlyAmount",
            example = "100000"
    )
    private BigDecimal accumulatedMonthlyAmount;

    private ProductLimitConfigEntity productLimitConfig;
}
