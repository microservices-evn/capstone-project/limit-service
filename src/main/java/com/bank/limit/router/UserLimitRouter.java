package com.bank.limit.router;

import com.bank.limit.service.UserLimitService;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.SagaPropagation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserLimitRouter extends RouteBuilder {
    @Autowired
    private UserLimitService userLimitService;

    @Override
    public void configure() throws Exception {
        from("jms:queue:{{services.limit.reserved}}")
                .routeId("reserved_user_limit")
                .saga()
                .propagation(SagaPropagation.MANDATORY)
                .compensation("direct:cancel_reserved_user_limit")
                .to("log:DEBUG?showBody=true&showHeaders=true&showProperties=true")
                .log(" - body: ${body}")
                .log("Executing saga with LRA ${header.Long-Running-Action}")
                .bean(userLimitService, "reserveLimit")
                .end();

        from("direct:cancel_reserved_user_limit")
                .routeId("cancel_reserved_user_limit")
                .to("log:DEBUG?showBody=true&showHeaders=true&showProperties=true")
                .bean(userLimitService, "rollbackLimit")
                .log("Transaction ${header.Long-Running-Action} has been cancelled due to integration failure: ${body}");
    }
}
