package com.bank.limit.repository;

import com.bank.limit.entity.DailyTransactionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

public interface DailyTransactionEntityRepository extends JpaRepository<DailyTransactionEntity, UUID> {
    Stream<DailyTransactionEntity> findByFromCifAndCreatedAtAfterOrderByCreatedAt(String fromCif, Instant after);

    Optional<DailyTransactionEntity> findBySagaId(String sagaId);
    
}