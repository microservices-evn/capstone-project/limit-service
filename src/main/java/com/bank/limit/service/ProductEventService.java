package com.bank.limit.service;

import com.bank.limit.entity.ProductEventEntity;
import com.bank.limit.entity.ProductLimitConfigEntity;
import com.bank.limit.repository.ProductEventEntityRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
public class ProductEventService {
    @Value("${limit.kafka.topic.product-limit}")
    private String productEventTopicName;

    @Autowired
    private ProductLimitConfigService productLimitConfigService;
    @Autowired
    private ProductEventEntityRepository productEventEntityRepository;

    @KafkaListener(topics = "${limit.kafka.topic.product-limit}")
    void productLimitListener(String message) {
        log.info("recieved message: - {}", message);
        ProductEventEntity event = readEvent(message);
        if (null == event) {
            log.error("Can't read kafka event in topic + " + productEventTopicName);
            return;
        }
        log.info("read event of: " + event.getProductId());
        log.info("limit type is: " + event.getProductLimitType());
        log.info("new limit value is: " + event.getProductLimitAmount());
        saveEvent(event);
        productLimitConfigService.updateLimit(event);
    }

    @Transactional
    public void saveEvent(ProductEventEntity event) {
        productEventEntityRepository.save(event);
    }

    private ProductEventEntity readEvent(String message) {
        ProductEventEntity event = null;
        JsonNode productNode = null;
        try {
            productNode = new ObjectMapper().readTree(message).get("after");
            event = new ProductEventEntity();
            event.setEventId(UUID.fromString(productNode.get("event_id").asText()));
            event.setProductId(UUID.fromString(productNode.get("product_id").asText()));
            event.setProductCode(productNode.get("product_code").asText());
            event.setProductLimitType(productNode.get("product_limit_type").asText());
            event.setProductLimitAmount(BigDecimal.valueOf(
                    Long.parseLong(productNode.get("product_limit_amount").asText())));
            event.setEventCreatedAt(Instant.ofEpochMilli(
                    Long.parseLong(productNode.get("event_created_at").asText())));
            event.setProductLastUpdatedAt(Instant.ofEpochMilli(
                    Long.parseLong(productNode.get("product_last_updated_at").asText())));
            event.setEventCreatedAt(Instant.ofEpochMilli(
                    Long.parseLong(productNode.get("event_created_at").asText())));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return event;
    }

    public List<ProductEventEntity> retrieveProductLimitEvents() {
        return productEventEntityRepository.findAll(
                Sort.by(Sort.Direction.DESC, "eventCreatedAt")
        );
    }

    public static void main(String[] args) throws JsonProcessingException {
        String eventString = "{\"before\":null,\"after\":{\"event_id\":\"52189638-5b02-4a4a-8e93-10a2c0fa30e2\",\"product_id\":\"a51fea9c-ea7a-4a02-bfd4-0eec3aa55a5a\",\"product_code\":\"INTERNAL_TRANSFER\",\"product_limit_type\":\"DAILY_LIMIT\",\"product_limit_amount\":\"100000000\",\"product_last_updated_at\":1689243943100475,\"event_created_at\":1689243943100475},\"source\":{\"version\":\"2.1.4.Final\",\"connector\":\"postgresql\",\"name\":\"postgres\",\"ts_ms\":1689243943130,\"snapshot\":\"false\",\"db\":\"product_db\",\"sequence\":\"[\\\"62971664\\\",\\\"62971720\\\"]\",\"schema\":\"public\",\"table\":\"product_event\",\"txId\":1025,\"lsn\":62971720,\"xmin\":null},\"op\":\"c\",\"ts_ms\":1689243943563,\"transaction\":null}\n";
        JsonNode productNode = new ObjectMapper().readTree(eventString).get("after");

        System.out.println("product event_id = " + productNode.get("event_id").asText());
        System.out.println("product product_id = " + productNode.get("product_id").asText());
        System.out.println("product product_code = " + productNode.get("product_code").asText());
        System.out.println("product product_limit_type = " + productNode.get("product_limit_type").asText());
        System.out.println("product product_limit_amount = " + productNode.get("product_limit_amount").asText());
        System.out.println("product product_last_updated_at = " + productNode.get("product_last_updated_at").asText());
        System.out.println("product event_created_at = " + productNode.get("event_created_at").asText());

        UUID id = UUID.fromString(productNode.get("event_id").asText());
    }
}
