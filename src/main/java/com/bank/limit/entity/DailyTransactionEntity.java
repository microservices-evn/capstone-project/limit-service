package com.bank.limit.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.UUID;

@Entity
@Table(name = "daily_transactions")
public class DailyTransactionEntity {
    @Id
    @Column(name = "id", nullable = false)
    private UUID id;

    @Column(name = "transaction_id", nullable = false)
    private UUID transactionId;

    @Column(name = "from_cif", nullable = false)
    private String fromCif;

    @Column(name = "beneficiary", nullable = false)
    private String beneficiary;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "product_id")
    private String productId;

    @Column(name = "product_code")
    private String productCode;

    @Column(name = "status")
    private String status;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "saga_id")
    private String sagaId;

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getBeneficiary() {
        return beneficiary;
    }

    public void setBeneficiary(String beneficiary) {
        this.beneficiary = beneficiary;
    }

    public String getFromCif() {
        return fromCif;
    }

    public void setFromCif(String fromCif) {
        this.fromCif = fromCif;
    }

    public UUID getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(UUID transactionId) {
        this.transactionId = transactionId;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getSagaId() {
        return sagaId;
    }

    public void setSagaId(String sagaId) {
        this.sagaId = sagaId;
    }

    @Override
    public String toString() {
        return "DailyTransactionEntity{" +
                "id=" + id +
                ", transactionId=" + transactionId +
                ", fromCif='" + fromCif + '\'' +
                ", beneficiary='" + beneficiary + '\'' +
                ", amount=" + amount +
                ", productId='" + productId + '\'' +
                ", productCode='" + productCode + '\'' +
                ", status='" + status + '\'' +
                ", createdAt=" + createdAt +
                ", sagaId=" + sagaId +
                '}';
    }
}