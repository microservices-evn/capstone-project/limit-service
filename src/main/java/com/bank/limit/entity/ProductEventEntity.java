package com.bank.limit.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "product_event")
public class ProductEventEntity {
    @Id
    @Column(name = "event_id", updatable = false, nullable = false)
    private UUID eventId;

    @Column(name = "product_id")
    private UUID productId;

    @Column(name = "product_code", nullable = false)
    private String productCode;

    @Column(name = "product_limit_type", nullable = false)
    private String productLimitType;

    @Column(name = "product_limit_amount")
    private BigDecimal productLimitAmount;

    @Column(name = "product_last_updated_at")
    private Instant productLastUpdatedAt;

    @Column(name = "event_created_at")
    private Instant eventCreatedAt;
}